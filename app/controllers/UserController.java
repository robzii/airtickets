package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.interfaces.UserServiceInterface;
import views.dataForms.LoginForm;
import views.html.login;
import views.html.profile;

import javax.inject.Inject;
import java.util.Map;
import java.util.Optional;

/**
 * Created by erotpik on 2017-06-17.
 */
public class UserController extends Controller {
    HttpExecutionContext executionContext;

    FormFactory formFactory;

    UserServiceInterface userService;

    @Inject
    public UserController(HttpExecutionContext executionContext, FormFactory formFactory, UserServiceInterface userService) {
        this.executionContext = executionContext;
        this.formFactory = formFactory;
        this.userService = userService;
    }

    public Result login() {
        Form<LoginForm> formData = formFactory.form(LoginForm.class);
        return ok(login.render("Login", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()), formData));
    }

    public Result postLogin() {
        Form<LoginForm> formData = formFactory.form(LoginForm.class).bindFromRequest();

        if (formData.hasErrors()) {
            flash("error", "Login credentials not valid.");
            return badRequest(login.render("Login", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx()), formData));
        } else {
            session().clear();
            session("login", formData.get().login);
            return redirect(routes.UserController.profile());
        }

    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result register() {
        JsonNode jsonNode = Optional.ofNullable(request().body().asJson()).orElse(Json.parse("{}"));
        User user;
        try {
            user = userService.createUser(jsonNode);
        } catch (Exception e) {
            return badRequest();
        }

        return created("User created with id:" + user.getId());
    }

    @Security.Authenticated(Secured.class)
    public Result logout() {
        session().clear();
        return redirect(routes.HomeController.index());
    }

    @Security.Authenticated(Secured.class)
    public Result profile() {
        return ok(profile.render("Profile", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx())));
    }

}
