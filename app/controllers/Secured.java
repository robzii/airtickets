package controllers;

import models.User;
import play.mvc.Result;
import play.mvc.Http.Context;
import play.mvc.Security.Authenticator;

/**
 * Created by erotpik on 2017-06-17.
 */
public class Secured extends Authenticator {

    @Override
    public String getUsername(Context ctx) {
        return ctx.session().get("login");
    }


    @Override
    public Result onUnauthorized(Context context) {
        return redirect(routes.UserController.login());
    }


    public static String getUser(Context ctx) {
        return ctx.session().get("login");
    }


    public static boolean isLoggedIn(Context ctx) {
        return (getUser(ctx) != null);
    }

    public static User getUserInfo(Context ctx) {
        return (isLoggedIn(ctx) ? User.find.where().ilike("login", getUser(ctx)).findUnique() : null);
    }
}
