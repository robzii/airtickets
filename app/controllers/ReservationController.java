package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Reservation;
import models.User;
import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.interfaces.ReservationServiceInterface;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by erotpik on 2017-06-18.
 */
public class ReservationController extends Controller {

    ReservationServiceInterface reservationService;

    FormFactory formFactory;

    @Inject
    public ReservationController(ReservationServiceInterface reservationService, FormFactory formFactory) {
        this.reservationService = reservationService;
        this.formFactory = formFactory;
    }

    public Result reserveGuestTicket(){
        JsonNode jsonNode = Optional.ofNullable(request().body().asJson()).orElse(Json.toJson("{}"));
        Form<Reservation> form = formFactory.form(Reservation.class).bind(jsonNode);
        List<ValidationError> errors = reservationService.reserveForGuest(form.get());

        if(!errors.isEmpty()) {
                return badRequest(errors.stream().map(ValidationError::toString).reduce("",String::concat));
        }

        return created();
    }

    @Security.Authenticated(Secured.class)
    public Result reserveUserTicket(){
        JsonNode jsonNode = Optional.ofNullable(request().body().asJson()).orElse(Json.toJson("{}"));
        Form<Reservation> form = formFactory.form(Reservation.class).bind(jsonNode);
        List<ValidationError> errors = reservationService.reserveForUser(form.get(), Secured.getUserInfo(ctx()));

        if(!errors.isEmpty()) {
            return badRequest(errors.stream().map(ValidationError::toString).reduce("",String::concat));
        }

        return created();
    }

    @Security.Authenticated(Secured.class)
    public Result getReservations(){
        User user = Secured.getUserInfo(ctx());
        List<Reservation> reservationHistory = Reservation.find.where().eq("user_id",user.getId()).findList();
        return ok(Json.toJson(reservationHistory).toString());
    }

    @Security.Authenticated(Secured.class)
    public Result deleteReservation(Long id){
        List <ValidationError> errors = new ArrayList<>();
        errors.addAll(reservationService.delete(id, Secured.getUserInfo(ctx())));

        if(!errors.isEmpty()){
            return badRequest(errors.stream().map(ValidationError::toString).reduce("",String::concat));
        }

        return ok();
    }

    @Security.Authenticated(Secured.class)
    public Result update(Long id, Long flightId){
        List <ValidationError> errors = new ArrayList<>();
        errors.addAll(reservationService.update(id, flightId, Secured.getUserInfo(ctx())));

        if(!errors.isEmpty()){
            return badRequest(errors.stream().map(ValidationError::toString).reduce("",String::concat));
        }

        return  ok();
    }

    public Result pay(Long id){
        Reservation reservation = Reservation.find.byId(id);
        reservation.setPaymentStatus(true);
        reservation.save();
        return ok();
    }
}
