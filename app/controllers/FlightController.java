package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.*;
import services.interfaces.FlightServiceInterface;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 * Created by erotpik on 2017-06-14.
 */
public class FlightController extends Controller {

    HttpExecutionContext executionContext;

    FlightServiceInterface flightsService;

    @Inject
    public FlightController(HttpExecutionContext executionContext, FlightServiceInterface filghtsService) {
        this.executionContext = executionContext;
        this.flightsService = filghtsService;
    }

    @BodyParser.Of(BodyParser.Json.class)
    public CompletionStage<Result> find() {
        Optional<JsonNode> jsonP = Optional.ofNullable(request().body().asJson());
        Map<String, Object> criteria = new HashMap<String, Object>();
        jsonP.ifPresent(body -> criteria.putAll(Json.fromJson(body, Map.class)));

        return flightsService.find(criteria)
                             .thenApplyAsync(flightStream -> {
                                 return ok(Json.toJson(flightStream.collect(Collectors.toList())));
                             }, executionContext.current())
                             .exceptionally(flightStream -> {
                                 return badRequest("Bad Request. Bad key in query.");
                             });
    }

}
