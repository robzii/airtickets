package controllers;

import play.mvc.*;
import play.libs.concurrent.HttpExecutionContext;
import javax.inject.Inject;


public class HomeController extends Controller {

    HttpExecutionContext executionContext;

    @Inject
    public HomeController(HttpExecutionContext executionContext) {
        this.executionContext = executionContext;
    }

    public Result index() {
        return ok(views.html.index.render("Home", Secured.isLoggedIn(ctx()), Secured.getUserInfo(ctx())));
    }


}
