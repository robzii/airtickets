import com.google.inject.AbstractModule;
import services.*;
import services.interfaces.FlightServiceInterface;
import services.interfaces.PersonalDataSeriviceInterface;
import services.interfaces.ReservationServiceInterface;
import services.interfaces.UserServiceInterface;

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.
 * <p>
 * Play will automatically use any class called `Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
public class Module extends AbstractModule {

    @Override
    public void configure() {

        bind(FlightServiceInterface.class).to(FlightService.class);
        bind(UserServiceInterface.class).to(UserService.class);
        bind(PersonalDataSeriviceInterface.class).to(PersonalDataService.class);
        bind(ReservationServiceInterface.class).to(ReservationService.class);
    }

}
