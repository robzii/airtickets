package models;

import javax.persistence.*;


@Entity
public class User extends com.avaje.ebean.Model {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(unique = true)
    Long id;

    @Column(nullable = false, unique = true)
    String login;


    @Column(nullable = false)
    String password;

    @OneToOne
    PersonalData details;

    String accountType;


    public static Find<Long, User> find = new Find<Long, User>() {
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PersonalData getDetails() {
        return details;
    }

    public void setDetails(PersonalData details) {
        this.details = details;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
}