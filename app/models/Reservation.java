package models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import controllers.Secured;
import play.data.validation.ValidationError;
import play.mvc.WebSocket;

import static play.mvc.Http.Context.Implicit.ctx;


@Entity
public class Reservation extends com.avaje.ebean.Model {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    Long id;

    @Column(nullable = false)
    @ManyToOne()
    Flight flight;

    @ManyToOne
    User user;

    @ManyToOne
    public PersonalData personalData;

    @Column(columnDefinition = "tinyint(1) default 0")
    boolean paymentStatus;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm")
    LocalDateTime bookingDate;

    @Column(columnDefinition = "tinyint(1) default 1")
    boolean reservationStatus;

    @Column(nullable = false)
    Double price;

    public static Find<Long, Reservation> find = new Find<Long, Reservation>() {
    };

    public static Comparator<Reservation> maxID = (r1, r2) -> Long.compare(r1.getId(),r2.getId());

    public Long getId() {
        return id;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public LocalDateTime getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDateTime bookingDate) {
        this.bookingDate = bookingDate;
    }

    public boolean isReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(boolean reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public Double getPrice() {return price;}

    public void setPrice(Double price) {this.price = price;}
}