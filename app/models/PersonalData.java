package models;

import play.api.data.validation.ValidationError;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class PersonalData extends com.avaje.ebean.Model {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    Long id;

    @Column(nullable = false, unique = true)
    Long pesel;

    @Column(nullable = false)
    String name;

    @Column(nullable = false)
    String surename;

    @Column(nullable = false, unique = true)
    String email;

    String city;

    String street;

    String country;

    public static Find<Long, PersonalData> find = new Find<Long, PersonalData>() {
    };


    public Long getId() {
        return id;
    }

    public void setId(Long id) {this.id = id;}

    public Long getPesel() {
        return pesel;
    }

    public void setPesel(Long pesel) {
        this.pesel = pesel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurename() {
        return surename;
    }

    public void setSurename(String surename) {
        this.surename = surename;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}