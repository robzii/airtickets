package views.dataForms;

import play.data.validation.ValidationError;
import services.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by erotpik on 2017-06-17.
 */
public class LoginForm {

    public String login;

    public String password;

    public LoginForm() {

    }

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();
        if (!UserService.isValid(login, password)) {
            errors.add(new ValidationError("login", ""));
            errors.add(new ValidationError("password", ""));
        }
        return (errors.size() > 0) ? errors : null;
    }
}
