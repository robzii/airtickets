package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.PersonalData;
import play.libs.Json;
import services.interfaces.PersonalDataSeriviceInterface;

import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * Created by erotpik on 2017-06-17.
 */
public class PersonalDataService implements PersonalDataSeriviceInterface {

    @Override
    @NotNull
    public PersonalData createUserDetails(JsonNode jsonNode) {
        PersonalData personalData = Json.fromJson(jsonNode, PersonalData.class);
        personalData.save();

        return personalData;
    }

    @Override
    public boolean ifPersonalDataExist(Long id) {
        return Optional.ofNullable(id)
                       .filter(it -> PersonalData.find.byId(it) != null)
                       .isPresent();
    }

}
