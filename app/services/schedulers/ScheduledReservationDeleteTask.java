package services.schedulers;

import models.Reservation;

import java.time.LocalDateTime;
import java.util.List;
import java.util.TimerTask;

/**
 * Created by erotpik on 2017-06-19.
 */
public class ScheduledReservationDeleteTask extends TimerTask {
    @Override
    public void run() {
        List<Reservation> reservationList = Reservation.find.where().eq("payment_status", false)
                                                           .eq("reservation_status", true)
                                                           .findList();

        reservationList.stream().filter(it -> it.getFlight().getFlightDate().minusDays(new Long(1)).isAfter(LocalDateTime.now()))
                                .forEach(it -> {
                                    it.setReservationStatus(false);
                                    it.save();
                                });
    }
}
