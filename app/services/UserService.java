package services;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import models.User;
import models.PersonalData;
import play.libs.Json;
import services.interfaces.PersonalDataSeriviceInterface;
import services.interfaces.UserServiceInterface;

import javax.inject.Inject;
import java.util.Optional;

/**
 * Created by erotpik on 2017-06-17.
 */
public class UserService implements UserServiceInterface {

    PersonalDataSeriviceInterface personalDataService;

    @Inject
    public UserService(PersonalDataSeriviceInterface personalDataService){
        this.personalDataService = personalDataService;
    }

    static public User getUser(String login) {
        return User.find.where().ilike("login", login).findUnique();
    }


    static public boolean isUser(String login) {
        return User.find.where().ilike("login", login).findRowCount() == 1;
    }


    static public boolean isValid(String login, String password) {
        return ((login != null) && (password != null) && (isUser(login)) && (getUser(login).getPassword().equals(password)));
    }

    public User createUser(JsonNode jsonNode) {
        Optional<JsonNode> jsonUserDetails = Optional.ofNullable(jsonNode.findValue("details"));
        PersonalData personalData;
        User user;
        Ebean.beginTransaction();
        try {
            personalData = personalDataService.createUserDetails(jsonUserDetails.orElse(Json.parse("{}")));
            user = createUser(jsonNode, personalData);
            Ebean.commitTransaction();
        } finally {
            Ebean.endTransaction();
        }

        return user;
    }

    @Override
    public boolean isUserExist(Long id) {
        return Optional.ofNullable(id)
                       .map(it -> User.find.byId(id) != null)
                       .get();
    }


    private User createUser(JsonNode jsonNode, PersonalData personalData) {
        User user = Json.fromJson(jsonNode, User.class);
        user.setDetails(personalData);
        user.save();

        return user;
    }
}
