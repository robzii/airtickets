package services;

import models.Flight;
import services.executors.DatabaseExecutionContext;
import services.interfaces.FlightServiceInterface;


import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;


public class FlightService implements FlightServiceInterface {
    @Inject
    DatabaseExecutionContext executionContext;

    @Inject
    public FlightService(DatabaseExecutionContext executionContext) {
        this.executionContext = executionContext;
    }

    public CompletionStage<Stream<Flight>> find(Map<String, Object> criteria) {
        return supplyAsync(() -> findFlights(criteria), executionContext);
    }

    private static Stream<Flight> findFlights(Map<String, Object> criteria) {
        List<Flight> flightList = Flight.find.where()
                                             .allEq(criteria)
                                             .findList();
        return flightList.stream();
    }

    @Override
    public boolean isFlightExist(Long id) {
        return Optional.ofNullable(id)
                       .filter(it -> Flight.find.byId(id) != null)
                       .isPresent();
    }

}