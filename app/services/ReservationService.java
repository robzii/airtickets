package services;

import controllers.Secured;
import models.Flight;
import models.Reservation;
import models.User;
import play.data.validation.ValidationError;
import play.libs.Json;
import services.interfaces.FlightServiceInterface;
import services.interfaces.PersonalDataSeriviceInterface;
import services.interfaces.ReservationServiceInterface;
import services.interfaces.UserServiceInterface;
import services.schedulers.ScheduledReservationDeleteTask;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Timer;

import static play.mvc.Http.Context.Implicit.ctx;

/**
 * Created by erotpik on 2017-06-18.
 */
public class ReservationService implements ReservationServiceInterface {


    UserServiceInterface userService;


    PersonalDataSeriviceInterface personalDataService;


    FlightServiceInterface flightService;

    @Inject
    public ReservationService(UserServiceInterface userService, PersonalDataSeriviceInterface personalDataService, FlightServiceInterface flightService) {
        this.userService = userService;
        this.personalDataService = personalDataService;
        this.flightService = flightService;

/*        Timer time = new Timer();
        ScheduledReservationDeleteTask reservationDeleteScheduler = new ScheduledReservationDeleteTask();
        int dayInSecond = 86400;
        time.schedule(reservationDeleteScheduler, 0, dayInSecond);*/
    }

    public List<ValidationError> reserveForGuest(Reservation reservation) {
        reservation.setUser(null);
        reservation.setBookingDate(LocalDateTime.now());
        List<ValidationError> errors = validateReservationForGuest(reservation);

        if (errors.isEmpty()) {
            Long flightId = reservation.getFlight().getId();
            reservation.setPrice(Flight.find.byId(flightId).getPrice().doubleValue());
            reservation.save();
        }

        return errors;
    }


    public List<ValidationError> reserveForUser(Reservation reservation, User sessionUser) {
        reservation.setPersonalData(null);
        reservation.setUser(sessionUser);
        reservation.setBookingDate(LocalDateTime.now());
        List<ValidationError> errors = validateReservationForUser(reservation, sessionUser);

        if (errors.isEmpty()) {
            Long flightId = reservation.getFlight().getId();
            reservation.setPrice(calculateTicketPrice(reservation.getUser(), Flight.find.byId(flightId)));
            reservation.save();
        }

        return errors;
    }

    @Override
    public List<ValidationError> delete(Long id,User sessionUser) {
        Reservation reservation = Reservation.find.byId(id);
        List<ValidationError> errors = new ArrayList<>();
        errors.addAll(validateReservationToDelete(reservation, sessionUser));
        if (errors.isEmpty()) {
            reservation.setReservationStatus(false);
            reservation.save();
        }
        return errors;
    }

    @Override
    public List<ValidationError> update(Long id, Long flightId, User sessionUser) {
        Reservation reservation = Reservation.find.byId(id);
        List<ValidationError> errors = new ArrayList<>();
        errors.addAll(validateReservationToUpdate(reservation, sessionUser));
        reservation.setFlight(Flight.find.byId(flightId));
        errors.addAll(validateReservation(reservation));

        if (errors.isEmpty()) {
            reservation.setPrice(calculateTicketPrice(reservation.getUser(), reservation.getFlight()));
            reservation.save();
        }

        return errors;
    }

    private List<ValidationError> validateReservationForGuest(Reservation reservation) {
        List<ValidationError> errors = new ArrayList<>();

        errors.addAll(validateReservation(reservation));

        if (!personalDataService.ifPersonalDataExist(reservation.getPersonalData().getId())) {
            errors.add(new ValidationError("PersonalData", "Personal Data non exist"));
        }

        return errors.size() > 0 ? errors : Collections.emptyList();
    }

    private List<ValidationError> validateReservationForUser(Reservation reservation, User sessionUser) {
        List<ValidationError> errors = new ArrayList<>();

        errors.addAll(validateReservation(reservation));

        if (sessionUser == null) {
            errors.add(new ValidationError("Sesion", "Sesion Inactive"));
        }
        else if (!userService.isUserExist(reservation.getUser().getId())) {
            errors.add(new ValidationError("User", "User non exist"));
        }


        return errors.size() > 0 ? errors : Collections.emptyList();
    }

    private List<ValidationError> validateReservation(Reservation reservation) {
        List<ValidationError> errors = new ArrayList<>();

        if (reservation.getFlight() != null && !flightService.isFlightExist(reservation.getFlight().getId())) {
            errors.add(new ValidationError("Flight", "Flight no exist"));
        } else if (!isNotToLateToReservation(reservation.getBookingDate(), reservation.getFlight())) {
            errors.add(new ValidationError("Date", "To late to rezervation fligt"));
        }

        return errors.size() > 0 ? errors : Collections.emptyList();
    }

    private List<ValidationError> validateReservationToDelete(Reservation reservation, User sessionUser) {
        List<ValidationError> errors = new ArrayList<>();

        if(reservation == null){
            errors.add(new ValidationError("Reservation", "Reservation no exist"));
        }
        else if (!isNotToLateToCancelReservation(LocalDateTime.now(), reservation.getFlight())) {
            errors.add(new ValidationError("Date", "Is to late for remove ticket"));
        }
        else if (!sessionUser.getLogin().equals(reservation.getUser().getLogin())) {
            errors.add(new ValidationError("Sesion", "You don't have a permission"));
        }

        return errors.size() > 0 ? errors : Collections.emptyList();
    }

    private List<ValidationError> validateReservationToUpdate(Reservation reservation, User sessionUser) {
        List<ValidationError> errors = new ArrayList<>();


        if (!isNotToLateToUpdateReservation(LocalDateTime.now(), reservation.getFlight())) {
            errors.add(new ValidationError("Date", "Is to late for update ticket"));
        }
        if (!sessionUser.getLogin().equals(reservation.getUser().getLogin())) {
            errors.add(new ValidationError("Sesion", "You don't have a permission to update this reservation"));
        }

        return errors.size() > 0 ? errors : Collections.emptyList();
    }


    public boolean isNotToLateToReservation(LocalDateTime reservationDate, Flight flight) {
        return Optional.ofNullable(flight)
                       .filter(it -> Flight.find.byId(flight.getId()).getFlightDate().isAfter(reservationDate))
                       .isPresent();
    }

    public boolean isNotToLateToCancelReservation(LocalDateTime cancelationDate, Flight flight) {
        return Optional.ofNullable(flight)
                       .filter(it -> Flight.find.byId(flight.getId()).getFlightDate().isAfter(cancelationDate.plusDays(3)))
                       .isPresent();
    }

    public boolean isNotToLateToUpdateReservation(LocalDateTime updateDate, Flight flight) {
        return Optional.ofNullable(flight)
                       .filter(it -> Flight.find.byId(flight.getId()).getFlightDate().isAfter(updateDate.plusDays(2)))
                       .isPresent();
    }

    public double calculateTicketPrice(User user, Flight flight) {
        Double price = (double) flight.getPrice();
        double calculatedPrice = "vip".equals(user.getAccountType().toLowerCase())
                ? ((price > 1000) ? (price * 0.95) : price)
                : price;
        return calculatedPrice;
    }

}
