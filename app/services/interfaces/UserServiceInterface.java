package services.interfaces;

import com.fasterxml.jackson.databind.JsonNode;
import models.User;


/**
 * Created by erotpik on 2017-06-17.
 */
public interface UserServiceInterface {

    User createUser(JsonNode jsonNode);

    boolean isUserExist(Long id);
}
