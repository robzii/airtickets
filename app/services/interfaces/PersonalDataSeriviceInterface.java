package services.interfaces;

import com.fasterxml.jackson.databind.JsonNode;
import models.PersonalData;
import play.data.validation.ValidationError;

import java.util.List;

/**
 * Created by erotpik on 2017-06-17.
 */
public interface PersonalDataSeriviceInterface {

    PersonalData createUserDetails(JsonNode jsonNode);

    boolean ifPersonalDataExist(Long id);
}
