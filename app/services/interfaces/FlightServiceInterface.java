package services.interfaces;

import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import models.*;

public interface FlightServiceInterface {
    CompletionStage<Stream<Flight>> find(Map<String, Object> criteria);

    boolean isFlightExist(Long id);
}