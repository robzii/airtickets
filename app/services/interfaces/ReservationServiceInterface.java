package services.interfaces;

import models.Flight;
import models.Reservation;
import models.User;
import play.data.validation.ValidationError;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

/**
 * Created by erotpik on 2017-06-18.
 */
public interface ReservationServiceInterface {

    List<ValidationError> reserveForGuest(Reservation reserve);

    List<ValidationError> reserveForUser(Reservation reserve, User sessionUser);

    List<ValidationError> delete(Long id, User sessionUser);

    List<ValidationError> update(Long id, Long flightId, User sessionUser);

    boolean isNotToLateToReservation(LocalDateTime reservationDate, Flight flight);

    boolean isNotToLateToCancelReservation(LocalDateTime cancelationDate, Flight flight);

    boolean isNotToLateToUpdateReservation(LocalDateTime updateDate, Flight flight);

    double calculateTicketPrice(User user, Flight flight);

}
