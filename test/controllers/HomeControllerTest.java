package controllers;


import org.junit.runner.RunWith;

import play.Application;
import play.inject.guice.GuiceApplicationBuilder;

import play.mvc.Http.RequestBuilder;
import play.mvc.Result;
import play.test.WithApplication;
import com.googlecode.zohhak.api.Configure;
import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;

import static org.junit.Assert.assertEquals;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

@RunWith(ZohhakRunner.class)
@Configure(separator = ";")
public class HomeControllerTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .configure("db.default.url", "jdbc:mysql://localhost:3306/test")
                .build();
    }

    @TestWith({"/"})
    public void testIndex(String value) {

        RequestBuilder request = new RequestBuilder()
                .method(GET)
                .uri(value);

        Result result = route(app, request);
        assertEquals("Check response status", OK, result.status());
    }
}
