package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.zohhak.api.Configure;
import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import models.Reservation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.*;
import static play.test.Helpers.*;

/**
 * Created by erotpik on 2017-06-21.
 */
@RunWith(ZohhakRunner.class)
@Configure(separator = ";")
public class ReservationControllerTest extends WithApplication {

    public Http.Session sesion;

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .configure("db.default.url", "jdbc:mysql://localhost:3306/test")
                .build();
    }

    @Before
    public void prepareSesion() {
        Map<String, String> auth = new HashMap<>();
        auth.put("login", "vip");
        auth.put("password", "poprawne");

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/user/login")
                .bodyForm(auth);

        final Result result = route(app, request);
        sesion = result.session();
    }

    @TestWith("{\"flight.id\":4,\"personalData.id\":2}")
    public void reservationTicketForGuest(String json) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/ticket/reserveGuestTicket")
                .bodyJson(Json.parse(json));

        final Result result = route(app, request);
        List<Reservation> reservations = Reservation.find.where().findList();


        Reservation reservation = reservations.stream().max(Reservation.maxID).get();

        assertEquals(reservation.getFlight().getId(), Long.valueOf(4));
        assertEquals(reservation.getPersonalData().getId(), Long.valueOf(2));
        assertEquals(reservation.getBookingDate().getDayOfYear(), LocalDateTime.now().getDayOfYear());
        assertEquals(reservation.getUser(), null);
        assertEquals(result.status(), CREATED);

    }

    @TestWith("{\"flight.id\":1,\"personalData.id\":2}")
    public void reservationOutdatedTicketForGuest(String json) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/ticket/reserveGuestTicket")
                .bodyJson(Json.parse(json));

        final Result result = route(app, request);
        String body = contentAsString(result);

        assertTrue(body.contains("To late to rezervation fligt"));
        assertEquals(result.status(), BAD_REQUEST);

    }

    @TestWith("{\"flight.id\":0,\"personalData.id\":2}")
    public void reservationTicketForGuestAndNonExistFlight(String json) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/ticket/reserveGuestTicket")
                .bodyJson(Json.parse(json));

        final Result result = route(app, request);
        String body = contentAsString(result);

        assertTrue(body.contains("Flight no exist"));
        assertEquals(result.status(), BAD_REQUEST);

    }

    @TestWith("{\"flight.id\":4,\"personalData.id\":0}")
    public void reservationTicketForNonExistGuest(String json) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/ticket/reserveGuestTicket")
                .bodyJson(Json.parse(json));

        final Result result = route(app, request);
        String body = contentAsString(result);

        assertTrue(body.contains("Personal Data non exist"));
        assertEquals(result.status(), BAD_REQUEST);

    }

    @TestWith("{\"flight.id\":4}")
    public void reservationTicketForRegisterUser(String json) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/ticket/reserveUserTicket")
                .bodyJson(Json.parse(json))
                .session(sesion);

        final Result result = route(app, request);

        assertEquals(result.status(), CREATED);

    }

    @TestWith("{\"flight.id\":4}")
    public void reservationTicketPriceForRegisterVipUser(String json) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/ticket/reserveUserTicket")
                .bodyJson(Json.parse(json))
                .session(sesion);

        final Result result = route(app, request);
        List<Reservation> reservations = Reservation.find.where().findList();
        Reservation reservation = reservations.stream().max(Reservation.maxID).get();

        assertEquals(reservation.getPrice(), Double.valueOf(1235));
        assertEquals(result.status(), CREATED);

    }

    @TestWith("{\"flight.id\":4}")
    public void reservationTicketForRegisterUserWithoutSesion(String json) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/ticket/reserveUserTicket")
                .bodyJson(Json.parse(json));

        final Result result = route(app, request);

        assertEquals(result.status(), SEE_OTHER);

    }

    @TestWith("{\"flight.id\":0}")
    public void reservationTicketForRegisterUserAndNonExistFlight(String json) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/ticket/reserveUserTicket")
                .bodyJson(Json.parse(json))
                .session(sesion);

        final Result result = route(app, request);

        assertEquals(result.status(), BAD_REQUEST);

    }

    @TestWith("{\"flight.id\":1}")
    public void reservationTicketForRegisterUserAndOutdatedFlight(String json) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/ticket/reserveUserTicket")
                .bodyJson(Json.parse(json))
                .session(sesion);

        final Result result = route(app, request);
        String body = contentAsString(result);

        assertTrue(body.contains("To late to rezervation fligt"));
        assertEquals(result.status(), BAD_REQUEST);

    }

    @Test
    public void getReservationListForAuthUser() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .header("Content-Type", "application/json")
                .uri("/ticket/getReservations")
                .session(sesion);

        final Result result = route(app, request);
        JsonNode response = Json.parse(contentAsString(result));
        List<Reservation> reservations = Json.fromJson(response, List.class);

        assertEquals(reservations.size(), 3);
        assertEquals(result.status(), OK);

    }

    @Test
    public void getReservationListWithoutSession() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .header("Content-Type", "application/json")
                .uri("/ticket/getReservations");

        final Result result = route(app, request);

        assertEquals(result.status(), SEE_OTHER);

    }

    @TestWith("1")
    public void deleteReservation(Long reservationId) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .uri("/ticket/delete/" + reservationId)
                .session(sesion);

        final Result result = route(app, request);
        Reservation reservation = Reservation.find.byId(reservationId);

        assertEquals(result.status(), OK);
        assertEquals(reservation.isReservationStatus(), false);

    }

    @TestWith("3")
    public void deleteReservationIfUserUnauthorized(Long reservationId) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .uri("/ticket/delete/" + reservationId);

        final Result result = route(app, request);

        Reservation reservation = Reservation.find.byId(reservationId);

        assertEquals(result.status(), SEE_OTHER);
        assertEquals(reservation.isReservationStatus(), true);
    }

    @TestWith("3")
    public void deleteReservationOtherUserThanInSession(Long reservationId) {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .uri("/ticket/delete/" + reservationId)
                .session(sesion);

        final Result result = route(app, request);

        Reservation reservation = Reservation.find.byId(reservationId);

        assertEquals(result.status(), BAD_REQUEST);
        assertEquals(reservation.isReservationStatus(), true);

    }

}
