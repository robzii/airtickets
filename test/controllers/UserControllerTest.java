package controllers;

import com.googlecode.zohhak.api.Configure;
import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import org.junit.Before;
import org.junit.Test;
import play.mvc.Http.RequestBuilder;
import org.junit.runner.RunWith;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static play.test.Helpers.*;
import static play.mvc.Http.Status.BAD_REQUEST;
import static play.test.Helpers.route;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by erotpik on 2017-06-18.
 */

@RunWith(ZohhakRunner.class)
@Configure(separator = ";")
public class UserControllerTest extends WithApplication {

    public Http.Session sesion;

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .configure("db.default.url", "jdbc:mysql://localhost:3306/test")
                .build();
    }

    @Before
    public void prepareSession() {
        Map<String, String> auth = new HashMap<>();
        auth.put("login", "vip");
        auth.put("password", "poprawne");

        Http.RequestBuilder request = new RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/user/login")
                .bodyForm(auth);


        final Result result = route(app, request);
        sesion = result.session();
    }

    /*
        @FullJson
        @Only Needed
        @Not All Needed Not Unique Pesel without email
        @Not Unique Pesel
        @Not Unique Login
        @Not Unique Email
     */
    @TestWith({
            "{\"login\":\"robzii4\",\"password\":\"poprawne2\",\"details\":{\"pesel\":\"95050208731\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz5i@gmail.com\",\"city\":\"LDZ\",\"street\":\"Kaktusowa\",\"country\":\"POLAND\"},\"accountType\":\"ADMIN\"};" + CREATED + "",
            "{\"login\":\"robzii5\",\"password\":\"poprawne2\",\"details\":{\"pesel\":\"95150208732\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz0i@gmail.com\"}};" + CREATED + "",
            "{\"login\":\"robzii6\",\"password\":\"poprawne2\",\"details\":{\"pesel\":\"95050208731\",\"name\":\"Robert\",\"surename\":\"Nowak\"}};" + BAD_REQUEST + "",
            "{\"login\":\"robzii7\",\"password\":\"poprawne2\",\"details\":{\"pesel\":\"95050208731\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz01@gmail.com\"}};" + BAD_REQUEST + "",
            "{\"login\":\"robzii4\",\"password\":\"poprawne2\",\"details\":{\"pesel\":\"95050208736\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz55@gmail.com\"}};" + BAD_REQUEST + "",
            "{\"login\":\"robzii8\",\"password\":\"poprawne2\",\"details\":{\"pesel\":\"97050208327\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz5i@gmail.com\"}};" + BAD_REQUEST + ""
    })
    public void testRun(String json, int status) {
        Http.RequestBuilder request = new RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/user/register")
                .bodyJson(Json.parse(json));

        final Result result = route(app, request);

        assertEquals("Response Status: ", status, result.status());
    }

    @Test
    public void testGetLoginPage() {
        Http.RequestBuilder request = new RequestBuilder()
                .method(GET)
                .uri("/user/login");

        final Result result = route(app, request);

        assertEquals("Response Status: ", result.status(), OK);
    }

    @TestWith({"vip ; poprawne",
            "normal ; poprawne"})
    public void authTest(String login, String password) {
        Map<String, String> auth = new HashMap<>();
        auth.put("login", login);
        auth.put("password", password);

        Http.RequestBuilder request = new RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/user/login")
                .bodyForm(auth);

        final Result result = route(app, request);

        assertEquals(result.status(), SEE_OTHER);
        assertEquals(result.session().isEmpty(), false);
        assertEquals(result.session().get("login"), login);
    }

    @TestWith({"badLogin ; poprawne",
            "normal ; badPassword"})
    public void authTestFailure(String login, String password) {
        Map<String, String> auth = new HashMap<>();
        auth.put("login", login);
        auth.put("password", password);

        Http.RequestBuilder request = new RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/user/login")
                .bodyForm(auth);


        final Result result = route(app, request);

        assertEquals(result.status(), BAD_REQUEST);
        assertEquals(result.session().isEmpty(), true);
    }

    @Test
    public void profileTest() {

        Http.RequestBuilder request = new RequestBuilder()
                .method(GET)
                .header("Content-Type", "application/json")
                .uri("/user/profile")
                .session(sesion);

        final Result result = route(app, request);

        assertEquals(result.status(), OK);
        assertEquals(result.session().isEmpty(), true);
    }

    @Test
    public void logoutTest() {

        Http.RequestBuilder request = new RequestBuilder()
                .method(GET)
                .header("Content-Type", "application/json")
                .uri("/user/logout")
                .session(sesion);

        final Result result = route(app, request);

        assertEquals(result.status(), SEE_OTHER);
        assertEquals(result.session().isEmpty(), true);
    }


}
