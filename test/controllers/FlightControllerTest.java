package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.zohhak.api.Configure;
import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http.RequestBuilder;
import play.mvc.Result;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

@RunWith(ZohhakRunner.class)
@Configure(separator = ";")
public class FlightControllerTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .configure("db.default.url", "jdbc:mysql://localhost:3306/test")
                .build();
    }


    @TestWith({
            "{};                                                                                          " + OK + ";    \"id\":2;                5",
            "{\"id\":1};                                                                                  " + OK + ";    \"flightNumber\":432;    1",
            "{\"startPlace\":\"WAW\",\"destinationPlace\":\"DUS\", \"flightDate\":\"2017-06-10 08:20\"};  " + OK + ";    \"flightDuration\":1000; 1",
            "{\"startPlace\":\"WAW\",\"destinationPlace\":\"WAW\", \"flightDate\":\"2017-06-10 08:20\"};  " + OK + ";    [];                      0"})

    public void testResultFlightFinder(String json, int status, String property, int objectAmount) {
        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/flight/find")
                .bodyJson(Json.parse(json));

        final Result result = route(app, request);
        final String body = contentAsString(result);
        final JsonNode bodyJson = Json.parse(body);


        assertEquals("Response Status: ", status, result.status());
        assertTrue("Check response content: ", body.contains(property));
        assertEquals("Objects amount: ", bodyJson.size(), objectAmount);
    }


    public void testBehaviorMetodForBadKey() {
        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/flight/find")
                .bodyJson(Json.parse("{\"idik\":0}"));

        final Result result = route(app, request);
        final String body = contentAsString(result);


        assertEquals("Response Status: ", BAD_REQUEST, result.status());
        assertTrue("Check response content:", body.contains("Bad Request. Bad key in query."));
    }

    @Test
    public void testBehaviorMetodForEmptyRequest() {
        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .header("Content-Type", "application/json")
                .uri("/flight/find");

        final Result result = route(app, request);
        final String body = contentAsString(result);


        assertEquals("Response Status: ", BAD_REQUEST, result.status());
        assertTrue("Check response content: ", body.contains("Bad Request"));

    }

    @Test
    public void testBehaviorMetodForBadTypeRequest() {
        RequestBuilder request = new RequestBuilder()
                .method(POST)
                .header("Content-Type", "text")
                .uri("/flight/find");

        final Result result = route(app, request);
        final String body = contentAsString(result);


        assertEquals("Response Status: ", UNSUPPORTED_MEDIA_TYPE, result.status());
        assertTrue("Check response content: ", body.contains("Bad Request"));

    }
}
