package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.zohhak.api.Configure;
import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import models.PersonalData;
import org.junit.Test;
import org.junit.runner.RunWith;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.test.WithApplication;
import services.interfaces.PersonalDataSeriviceInterface;

import javax.persistence.PersistenceException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by erotpik on 2017-06-23.
 */

@RunWith(ZohhakRunner.class)
@Configure(separator = ";")
public class PersonalDataServiceTest extends WithApplication {

    PersonalDataSeriviceInterface personalDataService = new PersonalDataService();

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .configure("db.default.url", "jdbc:mysql://localhost:3306/test")
                .build();
    }

    @TestWith({"1; true", "0; false"})
    public void checkIfPersonalDataExist(Long personalDataId, boolean expectedStatus) {
        boolean isExist = personalDataService.ifPersonalDataExist(personalDataId);

        assertEquals(isExist, expectedStatus);
    }


    @TestWith({"{\"pesel\":\"93050208536\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz5010@gmail.com\",\"city\":\"LDZ\",\"street\":\"Kaktusowa\",\"country\":\"POLAND\"}"})
    public void checkCreatePersonalData(String personalData) {
        JsonNode personalDataJson = Json.parse(personalData);
        PersonalData createdPersonalData = personalDataService.createUserDetails(personalDataJson);

        assertTrue(createdPersonalData.getEmail().equals(personalDataJson.get("email").textValue()));

    }

    @Test(expected = PersistenceException.class)
    public void checkCreatePersonalDataDuplicatedPesel() {
        String personalData = "{\"pesel\":\"93050208536\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz5030@gmail.com\",\"city\":\"LDZ\",\"street\":\"Kaktusowa\",\"country\":\"POLAND\"}";
        JsonNode personalDataJson = Json.parse(personalData);
        personalDataService.createUserDetails(personalDataJson);


    }

    @Test(expected = PersistenceException.class)
    public void checkCreatePersonalDataDuplicatedEmail() {
        String personalData = "{\"pesel\":\"93050208526\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz5010@gmail.com\",\"city\":\"LDZ\",\"street\":\"Kaktusowa\",\"country\":\"POLAND\"}";
        JsonNode personalDataJson = Json.parse(personalData);
        personalDataService.createUserDetails(personalDataJson);

    }

    @Test(expected = PersistenceException.class)
    public void checkCreatePersonalDataWithoutPeselAndEmail() {
        String personalData = "{\"name\":\"Robert\",\"surename\":\"Nowak\",\"city\":\"LDZ\",\"street\":\"Kaktusowa\",\"country\":\"POLAND\"}";
        JsonNode personalDataJson = Json.parse(personalData);
        personalDataService.createUserDetails(personalDataJson);

    }
}
