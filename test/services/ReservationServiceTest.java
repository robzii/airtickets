package services;

import com.avaje.ebean.Ebean;
import com.googlecode.zohhak.api.Configure;
import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import models.Flight;
import models.PersonalData;
import models.Reservation;
import models.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import play.Application;
import play.data.validation.ValidationError;
import play.db.ebean.Transactional;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.WithApplication;
import services.executors.DatabaseExecutionContext;
import services.interfaces.FlightServiceInterface;
import services.interfaces.PersonalDataSeriviceInterface;
import services.interfaces.ReservationServiceInterface;
import services.interfaces.UserServiceInterface;

import javax.inject.Inject;

import java.time.LocalDateTime;
import java.util.List;

import static com.thoughtworks.selenium.SeleneseTestBase.assertTrue;
import static com.thoughtworks.selenium.SeleneseTestNgHelper.assertEquals;

/**
 * Created by erotpik on 2017-06-23.
 */
@RunWith(ZohhakRunner.class)
@Configure(separator = ";")
public class ReservationServiceTest extends WithApplication {

    PersonalDataSeriviceInterface personalDataService = new PersonalDataService();

    UserServiceInterface userService = new UserService(personalDataService);

    @Inject
    DatabaseExecutionContext ec;
    FlightServiceInterface flightService = new FlightService(ec);

    ReservationServiceInterface reservationService = new ReservationService(userService, personalDataService, flightService);

    LocalDateTime now, twoDaysBeforeNow, threeDaysBeforeNow;
    Flight flight;

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .configure("db.default.url", "jdbc:mysql://localhost:3306/test")
                .build();
    }

    @Before
    public void prepareObjectsToTest() {
        Ebean.beginTransaction();

        now = LocalDateTime.now();
        twoDaysBeforeNow = now.minusDays(Long.valueOf(2));
        threeDaysBeforeNow = now.minusDays(Long.valueOf(3));

        flight = new Flight();
        flight.setDestinationPlace("waw");
        flight.setStartPlace("krk");
        flight.setFlightDate(now);
        flight.setFlightNumber(2452);
        flight.setPrice(1500);
        flight.setFlightDuration(230);
        flight.save();

    }

    @Test
    public void checkReservationForGuest() {
        Reservation r = new Reservation();
        List<ValidationError> errors;
        int reservationSize, reservationSizeAfter;

        r.setFlight(Flight.find.byId(Long.valueOf(4)));
        r.setPersonalData(PersonalData.find.byId(Long.valueOf(1)));

        reservationSize = Reservation.find.all().size();
        errors = reservationService.reserveForGuest(r);
        reservationSizeAfter = Reservation.find.all().size();
        Reservation re = Reservation.find.all().get(reservationSizeAfter - 1);

        assertEquals(reservationSize, reservationSizeAfter - 1);
        assertEquals(re.getPrice(), 1300.0);
        assertTrue(errors.isEmpty());
    }

    @Test
    public void checkReservaionForUser() {
        Reservation r = new Reservation();
        List<ValidationError> errors;
        int reservationsSize, reservationsSizeAfter;

        r.setFlight(Flight.find.byId(Long.valueOf(4)));
        r.setUser(User.find.byId(Long.valueOf(1)));

        reservationsSize = Reservation.find.all().size();
        errors = reservationService.reserveForUser(r, User.find.byId(Long.valueOf(1)));
        reservationsSizeAfter = Reservation.find.all().size();
        Reservation re = Reservation.find.all().get(reservationsSizeAfter - 1);

        assertEquals(reservationsSize, reservationsSizeAfter - 1);
        assertEquals(re.getPrice(), 1235.0);
        assertEquals(errors.size(), 0);
    }


    @Test
    @Transactional
    public void checkIsNotTooLateForReservationWhenFlightDeparted() {

        boolean isNotTooLate = reservationService.isNotToLateToReservation(now.plusHours(Long.valueOf(1)), flight);

        assertEquals(isNotTooLate, false);

    }

    @Test
    public void checkIsNotTooLateForReservationWhenFlightStarting() {

        boolean isNotTooLate = reservationService.isNotToLateToReservation(now, flight);

        assertEquals(isNotTooLate, false);

    }

    @Test
    public void checkIsNotTooLateForReservationWhenFlightWillStartForHalfHour() {

        boolean isNotTooLate = reservationService.isNotToLateToReservation(now.minusMinutes(Long.valueOf(30)), flight);

        assertEquals(isNotTooLate, true);

    }

    @Test
    public void checkIsNotTooLateForCancelReservation() {
        boolean isNotTooLate = reservationService.isNotToLateToCancelReservation(threeDaysBeforeNow.minusMinutes(Long.valueOf(30)), flight);

        assertEquals(isNotTooLate, true);
    }

    @Test
    public void checkIsNotTooLateForCancelReservationTwoDaysBeforeFlight() {
        boolean isNotTooLate = reservationService.isNotToLateToCancelReservation(twoDaysBeforeNow, flight);

        assertEquals(isNotTooLate, false);
    }

    @Test
    public void checkIsNotTooLateForCancelReservationThreeDaysBeforeFlight() {
        boolean isNotTooLate = reservationService.isNotToLateToCancelReservation(threeDaysBeforeNow, flight);

        assertEquals(isNotTooLate, false);
    }

    @Test
    public void checkIsNotTooLateForUpdateReservation() {
        boolean isNotTooLate = reservationService.isNotToLateToUpdateReservation(twoDaysBeforeNow.minusMinutes(Long.valueOf(30)), flight);

        assertEquals(isNotTooLate, true);
    }

    @Test
    public void checkIsNotTooLateForUpdateReservationTwoDaysBeforeFlight() {
        boolean isNotTooLate = reservationService.isNotToLateToUpdateReservation(twoDaysBeforeNow, flight);

        assertEquals(isNotTooLate, false);
    }

    @Test
    public void checkIsNotTooLateForUpdateReservationOneDayBeforeFlight() {
        boolean isNotTooLate = reservationService.isNotToLateToUpdateReservation(now.minusDays(Long.valueOf(1)), flight);

        assertEquals(isNotTooLate, false);
    }

    @TestWith({"1500; 1425", "1000; 1000", "800; 800"})
    public void calculateTicketPriceForVipUser(Integer flightPrice, Double expectedTicketPrice) {
        User vipUser = User.find.byId(Long.valueOf(1));
        flight.setPrice(flightPrice);

        Double calculatePrice = reservationService.calculateTicketPrice(vipUser, flight);

        assertEquals(calculatePrice, expectedTicketPrice);
    }

    @TestWith({"1500; 1500", "1000; 1000", "800; 800"})
    public void calculateTicketPriceForDefaultUser(Integer flightPrice, Double expectedTicketPrice) {
        User defaultUser = User.find.byId(Long.valueOf(2));
        flight.setPrice(flightPrice);

        Double calculatePrice = reservationService.calculateTicketPrice(defaultUser, flight);

        assertEquals(calculatePrice, expectedTicketPrice);
    }

    @Test
    public void deleteReservation() {
        List<ValidationError> errors;
        int reservationId = 3;
        int reservationOwner = 2;

        boolean isNotDeleteBeforeUseMethod = Reservation.find.byId(Long.valueOf(reservationId)).isReservationStatus();
        errors = reservationService.delete(Long.valueOf(reservationId), User.find.byId(Long.valueOf(reservationOwner)));
        boolean isNotDeleteAfterUseMethod = Reservation.find.byId(Long.valueOf(reservationId)).isReservationStatus();


        assertEquals(errors.size(), 0);
        assertEquals(isNotDeleteBeforeUseMethod, true);
        assertEquals(isNotDeleteAfterUseMethod, false);

    }

    @Test
    public void deleteReservationIfNoExist() {
        List<ValidationError> errors;
        errors = reservationService.delete(Long.valueOf(0), User.find.byId(Long.valueOf(2)));

        assertEquals(errors.size(), 1);
    }

    @Test
    public void deleteReservationWithoutAuthorization() {
        List<ValidationError> errors;
        errors = reservationService.delete(Long.valueOf(3), User.find.byId(Long.valueOf(1)));

        assertEquals(errors.size(), 1);
    }

    @Test
    public void checkUpdateReservation() {
        List<ValidationError> errors;
        Reservation updatedReservation;
        errors = reservationService.update(Long.valueOf(3), Long.valueOf(5), User.find.byId(Long.valueOf(2)));

        updatedReservation = Reservation.find.byId(Long.valueOf(3));

        assertEquals(errors.size(), 0);
        assertEquals(updatedReservation.getFlight().getId(), Long.valueOf(5));
    }

    @Test
    public void checkUpdateReservationForOutdatedFlight() {
        List<ValidationError> errors;
        errors = reservationService.update(Long.valueOf(3), Long.valueOf(1), User.find.byId(Long.valueOf(2)));

        assertEquals(errors.size(), 1);
    }

    @Test
    public void checkUpdateReservationForNonExistFlight() {
        List<ValidationError> errors;
        errors = reservationService.update(Long.valueOf(3), Long.valueOf(0), User.find.byId(Long.valueOf(2)));

        assertEquals(errors.size(), 1);
    }

    @After
    public void cancelTransactions() {
        Ebean.rollbackTransaction();
        Ebean.endTransaction();
    }

}
