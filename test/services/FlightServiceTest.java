package services;

import com.googlecode.zohhak.api.Configure;
import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import org.junit.runner.RunWith;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.WithApplication;
import services.executors.DatabaseExecutionContext;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;


/**
 * Created by erotpik on 2017-06-22.
 */
@RunWith(ZohhakRunner.class)
@Configure(separator = ";")
public class FlightServiceTest extends WithApplication {

    @Inject
    DatabaseExecutionContext executionContext;

    FlightService flightService = new FlightService(executionContext);

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .configure("db.default.url", "jdbc:mysql://localhost:3306/test")
                .build();
    }

    @TestWith({"2;true",
            "0;false"})
    public void testIsFlightExist(Long flightId, boolean isExist) {
        boolean isFlightExist = flightService.isFlightExist(flightId);

        assertEquals(isFlightExist, isExist);
    }

}
