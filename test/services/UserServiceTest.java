package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.zohhak.api.Configure;
import com.googlecode.zohhak.api.TestWith;
import com.googlecode.zohhak.api.runners.ZohhakRunner;
import models.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.test.WithApplication;
import services.interfaces.PersonalDataSeriviceInterface;

import javax.persistence.PersistenceException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by erotpik on 2017-06-22.
 */
@RunWith(ZohhakRunner.class)
@Configure(separator = ";")
public class UserServiceTest extends WithApplication {

    PersonalDataSeriviceInterface personalDataService = new PersonalDataService();
    UserService userS = new UserService(personalDataService);

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()
                .configure("db.default.url", "jdbc:mysql://localhost:3306/test")
                .build();
    }

    @TestWith({"2"})
    public void checkUserExist(Long userId) {
        boolean isExist = userS.isUserExist(Long.valueOf(userId));

        assertEquals(isExist, true);
    }

    @TestWith({"0"})
    public void checkUserForNonExist(Long userId) {
        boolean isExist = userS.isUserExist(Long.valueOf(userId));

        assertEquals(isExist, false);
    }

    @TestWith({"vip ; 1"})
    public void checkGetUser(String login, Long userId) {
        User user = userS.getUser(login);

        assertTrue(user.getLogin().equals(login));
        assertEquals(user.getId(), Long.valueOf(userId));
    }

    @TestWith({"nonExistUser"})
    public void checkGetNonExistUser(String login) {
        User user = userS.getUser(login);

        assertEquals(user, null);

    }

    @TestWith({"vip; true", "nonExist; false"})
    public void checkIsUserExist(String login, boolean expectedStatus) {
        boolean isUserExist = userS.isUser(login);

        assertEquals(isUserExist, expectedStatus);

    }

    @TestWith({"vip; poprawne; true",
            "nonExist; poprawne; false",
            "vip; badPassword; false",
            "nonExist; badPassword; false"})
    public void checkAuthorizationUser(String login, String password, boolean expectedStatus) {
        boolean authResult = userS.isValid(login, password);

        assertEquals(authResult, expectedStatus);

    }


    @TestWith("{\"login\":\"robzii10\",\"password\":\"poprawne2\",\"details\":{\"pesel\":\"93050208736\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz5i10@gmail.com\",\"city\":\"LDZ\",\"street\":\"Kaktusowa\",\"country\":\"POLAND\"},\"accountType\":\"ADMIN\"}")
    public void checkCreateUser(String json) {
        JsonNode userJson = Json.parse(json);

        User user = userS.createUser(userJson);

        assertTrue(user.getLogin().equals(userJson.get("login").textValue()));

    }

    @Test()
    public void checkCreateUserWithOnlyNeededData() {
        String json = "{\"login\":\"robzii15\",\"password\":\"poprawne2\",\"details\":{\"pesel\":\"91150208732\",\"name\":\"Robert\",\"surename\":\"Nowak\",\"email\":\"robiz071i@gmail.com\"}}";
        JsonNode userJson = Json.parse(json);
        User user = userS.createUser(userJson);
        assertTrue(user.getLogin().equals(userJson.get("login").textValue()));

    }

    @Test(expected = PersistenceException.class)
    public void checkCreateUserWithEmptyJson() {
        String json = "{}";
        JsonNode userJson = Json.parse(json);
        userS.createUser(userJson);

    }

    @Test(expected = PersistenceException.class)
    public void checkCreateUserWithWasteJson() {
        String json = "{\"login\":\"robzii6\",\"password\":\"poprawne2\",\"details\":{\"pesel\":\"95050208731\",\"name\":\"Robert\",\"surename\":\"Nowak\"}};";
        JsonNode userJson = Json.parse(json);
        userS.createUser(userJson);
    }

}
