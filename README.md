##To run this application you  need:##

mysql server(for example with xamp)  
https://www.apachefriends.org/pl/index.html

jdk 1.8

sbt(build tool for play)
http://www.scala-sbt.org/download.html

######RUN APP#############
1. Go to root project catalog
2. type sbt
3. start mysql server and create database `default` and `test`.   --
* create database default;
* create database test;
4. To run application type `run` or type `test` by test
5. After testing you must remove and create new database `test`.   --
* drop database test;
* create database test;

Exmple application run

![Play.png](https://bitbucket.org/repo/Mrnekon/images/4170679636-Play.png)



#BASIC USERS#

login: vip  password: poprawne

login: normal  password: poprawne

Main application page
localhost:port/


#Method for loged user

GET   /ticket/getReservations           --get list of reservations

POST  /ticket/delete/:id                --delete reservation

POST  /ticket/update/:id/:flightId      --update reservation

POST  /ticket/reserveUserTicket        --reserve ticket for loged user

GET   /user/logout                     --logout

GET   /user/profile                    --page for loged users

#Mothod for unauthenticated user

POST  /ticket/pay/:id                  --pay for ticket

POST  /ticket/reserveGuestTicket       --reservation for guest

POST  /flight/find                     --find tickets

POST   /user/register                  --register

GET    /user/login                     --to generate authorization form

POST   /user/login                     --to authentication user