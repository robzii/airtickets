# --- Sample dataset

# --- !Ups


INSERT INTO `flight` (`id`, `flight_number`, `start_place`, `destination_place`, `flight_date`, `flight_duration`, `price`) VALUES ('1', '432', 'Waw', 'DUS', '2017-06-10 08:20', '1000','200');
INSERT INTO `flight` (`id`, `flight_number`, `start_place`, `destination_place`, `flight_date`, `flight_duration`, `price`) VALUES ('2', '435', 'WAW', 'BDG', '2017-06-30 08:20', '800','800');
INSERT INTO `flight` (`id`, `flight_number`, `start_place`, `destination_place`, `flight_date`, `flight_duration`, `price`) VALUES ('3', '431', 'WRO', 'BDG', CURRENT_TIMESTAMP(), '500','120');
INSERT INTO `flight` (`id`, `flight_number`, `start_place`, `destination_place`, `flight_date`, `flight_duration`, `price`) VALUES ('4', '432', 'Waw', 'DUS', CURRENT_TIMESTAMP() + INTERVAL 5 DAY, '1000','1300');
INSERT INTO `flight` (`id`, `flight_number`, `start_place`, `destination_place`, `flight_date`, `flight_duration`, `price`) VALUES ('5', '432', 'Waw', 'SVA', CURRENT_TIMESTAMP() + INTERVAL 3 DAY, '1000','1300');


INSERT INTO  `personal_data` (`id`, `pesel`,`name`,`surename`,`email`,`city`,`street`,`country`) VALUES (1,91050201789,'Robert','Nowak','robzi@gmail.com','LDZ','Kaktusowa', 'POLAND');
INSERT INTO  `personal_data` (`id`, `pesel`,`name`,`surename`,`email`,`city`,`street`,`country`) VALUES (2,91050201790,'Robert','Nowak','robzii@gmail.com','LDZ','Kaktusowa', 'POLAND');
INSERT INTO  `personal_data` (`id`, `pesel`,`name`,`surename`,`email`,`city`,`street`,`country`) VALUES (3,91050201791,'Robert','Nowak','robziii@gmail.com','LDZ','Kaktusowa', 'POLAND');

INSERT INTO  `user` (`id`,`login`,`password`,`details_id`,`account_type`) values (1,'vip','poprawne',1,'VIP');
INSERT INTO  `user` (`id`,`login`,`password`,`details_id`,`account_type`) values (2,'normal','poprawne',3,'NORMAL');

INSERT INTO `reservation` (`id`, `flight_id`, `user_id`, `personal_data_id`, `payment_status`, `booking_date`,`price`,`reservation_status`) VALUES ('1', '4', '1', null, '0', '2017-06-15 08:20','1235',1);
INSERT INTO `reservation` (`id`, `flight_id`, `user_id`, `personal_data_id`, `payment_status`, `booking_date`,`price`,`reservation_status`) VALUES ('2', '4', null, '2', '0', '2017-06-15 08:20', '1300',1);
INSERT INTO `reservation` (`id`, `flight_id`, `user_id`, `personal_data_id`, `payment_status`, `booking_date`,`price`,`reservation_status`) VALUES ('3', '4', '2', null, '0', '2017-06-20 08:26','1235',1);