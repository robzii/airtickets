# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table flight (
  id                            bigint auto_increment not null,
  flight_number                 integer,
  start_place                   varchar(255),
  destination_place             varchar(255),
  flight_date                   datetime(6),
  flight_duration               integer,
  price                         integer,
  constraint pk_flight primary key (id)
);

create table personal_data (
  id                            bigint auto_increment not null,
  pesel                         bigint not null,
  name                          varchar(255) not null,
  surename                      varchar(255) not null,
  email                         varchar(255) not null,
  city                          varchar(255),
  street                        varchar(255),
  country                       varchar(255),
  constraint uq_personal_data_pesel unique (pesel),
  constraint uq_personal_data_email unique (email),
  constraint pk_personal_data primary key (id)
);

create table reservation (
  id                            bigint auto_increment not null,
  flight_id                     bigint,
  user_id                       bigint,
  personal_data_id              bigint,
  payment_status                tinyint(1) default 0,
  booking_date                  datetime(6) not null,
  reservation_status            tinyint(1) default 1,
  price                         double not null,
  constraint pk_reservation primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  login                         varchar(255) not null,
  password                      varchar(255) not null,
  details_id                    bigint,
  account_type                  varchar(255),
  constraint uq_user_login unique (login),
  constraint uq_user_details_id unique (details_id),
  constraint pk_user primary key (id)
);

alter table reservation add constraint fk_reservation_flight_id foreign key (flight_id) references flight (id) on delete restrict on update restrict;
create index ix_reservation_flight_id on reservation (flight_id);

alter table reservation add constraint fk_reservation_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_reservation_user_id on reservation (user_id);

alter table reservation add constraint fk_reservation_personal_data_id foreign key (personal_data_id) references personal_data (id) on delete restrict on update restrict;
create index ix_reservation_personal_data_id on reservation (personal_data_id);

alter table user add constraint fk_user_details_id foreign key (details_id) references personal_data (id) on delete restrict on update restrict;


# --- !Downs

alter table reservation drop foreign key fk_reservation_flight_id;
drop index ix_reservation_flight_id on reservation;

alter table reservation drop foreign key fk_reservation_user_id;
drop index ix_reservation_user_id on reservation;

alter table reservation drop foreign key fk_reservation_personal_data_id;
drop index ix_reservation_personal_data_id on reservation;

alter table user drop foreign key fk_user_details_id;

drop table if exists flight;

drop table if exists personal_data;

drop table if exists reservation;

drop table if exists user;

