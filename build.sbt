name := """AirTickets"""
organization := "com.robertp"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava,PlayEbean)

scalaVersion := "2.11.11"

libraryDependencies += jdbc
libraryDependencies += filters
libraryDependencies += "com.adrianhurt" %% "play-bootstrap" % "1.0-P25-B3"
libraryDependencies += evolutions
libraryDependencies +="com.googlecode.zohhak" % "zohhak" % "1.1.1"
libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "5.1.27"
)